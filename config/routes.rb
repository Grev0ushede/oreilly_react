Rails.application.routes.draw do
  get 'restauraunts/index'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :restaurants
  get '/restaurants', to: 'restaurants#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
